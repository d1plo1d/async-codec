<!-- cargo-sync-readme start -->

# Async Encode/Decode helpers

For prior art, see [tokio-codec](https://crates.io/crates/tokio-codec).
This borrows many of the ideas that originated there, though with a simpler
and `no_std`-compatible approach. No allocation is strictly required by
implementers of the [Encode] or [Decode] traits, making them suitable for
embedded applications.

## Encoders

Encoders are types that implement the [Encode] trait. Consumers of this
trait will provide an `Item` to be encoded, along with a buffer into which
the resulting bytes should be placed. The `Encode`r may return one of three
from this operation:

* `EncodeResult::Ok(n)`: The item was successfully encodeded into the
  provided buffer and took `n` bytes.
* `EncodeResult::Err(e)`: There was an unrecoverable error encoding the
  item. Subsequent calls with the same item should return the same error.
* `EncodeResult::Overflow(n)`: The item could be encoded, but the encoded
  representation would overflow the buffer. If `n > 0`, then a buffer of
  length `n` is required to hold the serialized item.

Upon an `Overflow` result, the caller should allocate a bigger buffer (if
possible), and attempt the operation again. `Encode` implementations may
keep a previously encoded `Item` in an internal buffer to make re-attempted
encodings cheaper. If this approach is used, the [Encode::reset] method must
also be implemented.

## Decoders

Decoders implement the [Decode] trait. To decode an `Item`, callers provide
the decoder with a buffer containing bytes from the stream. The decoder
returns a `usize` indicating that it consumed some number of bytes from the
buffer, along with one of three results:

* `DecodeResult::Ok(item)`: The `item` was successfully decoded
* `DecodeResult::Err(e)`: An error `e` arose when decoding the item. This
  does not necessarily invalidate the stream.
* `DecodeResult::UnexpectedEnd`: Not enough data to decode yet, caller
  should read more and try again with more bytes.

Decoders are free to consume bytes in-line with returning `UnexpectedEnd`
results and keep a running internal buffer of what's been read so far, or
they may opt for consuming the bytes for an `Item` all at once. They should
consume bytes in the error case as well so that the caller doesn't provide
the same bad data twice.

## Framed

The [Framed] type provides a wrapper for `AsyncRead + AsyncWrite` streams
that applies an `Encode + Decode` object to create an async `Stream + Sink`
of `Item`s.

<!-- cargo-sync-readme end -->

[![version](https://img.shields.io/crates/v/async-codec.svg)](https://crates.io/crates/async-codec/)
[![documentation](https://docs.rs/async-codec/badge.svg)](https://docs.rs/async-codec/)
[![license](https://img.shields.io/crates/l/async-codec.svg)](https://crates.io/crates/async-codec/)

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or <http://www.apache.org/licenses/LICENSE-2.0>)
* MIT license ([LICENSE-MIT](LICENSE-MIT) or <http://opensource.org/licenses/MIT>)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.
